## Telepítés
### Tesztelve CakePHP 2.8.x

1. Submodule telepítése

git submodule add http://gitlab.eclickapi.com:81/cakephp-2/Plugin-RestApi.git ./app/Plugin/RestApi


2. A plugin beállítása
A ~/app/Config/bootstrap.php hozzá kell adni a következő sort:
 - CakePlugin::load('RestApi', array('routes'=>true, 'bootstrap'=>true));
 

Leírás:
RestApi/Config/bootstrap.php van egy konfig ami befolyásolja az api életét

Használat:

Érdemes a Plugin/RestApi/Controller/ApiController.php-t átmásolni a küldő Controller mappába mert ebbe minden Test cucc benne van

Ha át van másolva a Controller mappába akkor az URL-t ahogy elérjük az Api.json de ha meg dokumentum kell akkor Api.doc

http://example.hu/Api.json
http://example.hu/Api.doc
