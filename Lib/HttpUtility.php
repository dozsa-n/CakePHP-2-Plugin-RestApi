<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2015. 10. 28.
 * Time: 20:43
 */

class HttpUtility {
    public static function getFunctionString($methodPrefix, $action){
        if (NameFormatter::isCamelCase($action)) {
            $ccFunctionName = $methodPrefix . ucfirst($action);
            $usFunctionName = NameFormatter::toSnakeCase($ccFunctionName);
        }

        if (NameFormatter::isSnakeCase($action)) {
            $usFunctionName = $methodPrefix . "_" . $action;
            $ccFunctionName = NameFormatter::toCamelCase($usFunctionName);
        }
        return compact('usFunctionName', 'ccFunctionName');
    }

    public static function convertRestApiAllowFunction($allowArray = array()){
        $allow = array();
        foreach($allowArray as $method => $actions){
            foreach($actions as $action){
                $allowAction = self::getFunctionString($method, $action);
                $allow[] = $allowAction['usFunctionName'];
                $allow[] = $allowAction['ccFunctionName'];
            }
        }
        return $allow;
    }

    public static function getFuncArgNames($_class, $funcName, $required = false) {
        $class = new ReflectionClass($_class);
        $method = $class->getMethod($funcName);
        $params = $method->getParameters();

        $result = array();
        $requiredFileds = array();
        foreach ($params as $param) {
            $name = $param->getName();
            //debug($param->__toString());
            if($required) {
                if (false !== strpos($param->__toString(), '<required>')) {
                    $result[] = $name;
                    $requiredFileds[] = $name;
                } else {
                    $result[] = $name;
                }
            }else{
                $result[] = $name;
            }
        }
        if($required){
            return array(
                'params' => $result,
                'required' => $requiredFileds
            );
        }else {
            return $result;
        }
    }
}