<?php
/**
 * Created by PhpStorm.
 * User: dnorbert
 * Date: 15. 10. 30.
 * Time: 10:08
 */

App::uses('NameFormatter', 'RestApi.Lib/AbstractData');

class CallFunction {
    /**
     * HTTP Method fajták
     * @var array
     */
    protected $routePrefixes = array(
        'POST' => 'post',
        'GET' => 'get',
        'DELETE' => 'delete',
        'PUT' => 'put'
    );

    public static $options = array(
        'caseType' => NameFormatter::CAMEL_CASE,
        'functionArgs' => true,
        'actionName' => 'action'
    );

//    protected $ccFunctionName;
//    protected $usFunctionName;
    protected $functionName;

    protected $request;

    public function __construct($options = array(), RestApiRequest $request)
    {
        self::$options = array_merge(self::$options, $options);
        $this->request = $request;
    }

    public function build()
    {
        $this->loadFunctionNames();
        return $this->callFunction();
    }

    protected function checkParamArguments($params)
    {

        $allParams = array_keys($this->allParams);
        $missingParams = array_diff($params, $allParams);
        if (!empty($missingParams)) {
            throw new RestApiMissingParamsException($missingParams);
        }
    }

    protected function createSortArguments($params)
    {
        $result = array();
        foreach ($params as $key => $param) {
            $result[$param] = $this->allParams[$param];
        }
        return $result;
    }

    protected function callFunctionArguments()
    {
        $ccFunctionName = $this->ccFunctionName;
        $usFunctionName = $this->usFunctionName;

        if (method_exists($this, $ccFunctionName)) {
            $params = HttpUtility::getFuncArgNames($this, $ccFunctionName);
            $this->checkParamArguments($params);
            return call_user_func_array(array($this, $ccFunctionName), $this->createSortArguments($params));
        } else if (method_exists($this, $usFunctionName)) {
            $params = HttpUtility::getFuncArgNames($this, $usFunctionName);
            $this->checkParamArguments($params);
            return call_user_func_array(array($this, $usFunctionName), $this->createSortArguments($params));
        }
    }

    protected function callFunction()
    {
        if (self::$options['functionArgs']) {
            return $this->callFunctionArguments();
        } else {
            if (method_exists($this, $this->functionName)) {
                return call_user_func_array(array($this, $ccFunctionName), array($this->request->allParams));
            }else {
                throw new RestApiUndefinedFunctionException($this->request->action);
            }
        }
    }

    protected function loadFunctionNames()
    {
        $methodPrefix = $this->routePrefixes[$this->request->method];
        if (NameFormatter::isCamelCase($this->request->action)) {
            $this->functionName = $methodPrefix . ucfirst($this->request->action);
        }

        if (NameFormatter::isSnakeCase($this->request->action)) {
            $this->functionName = $methodPrefix . "_" . $this->request->action;
        }
    }
    /*
    protected function callFunction()
    {
        $ccFunctionName = $this->ccFunctionName;
        $usFunctionName = $this->usFunctionName;

        if (self::$options['functionArgs']) {
            return $this->callFunctionArguments();
        } else {
            if (method_exists($this, $ccFunctionName)) {
                return call_user_func_array(array($this, $ccFunctionName), array($this->request->allParams));
            } else if (method_exists($this, $usFunctionName)) {
                return call_user_func_array(array($this, $usFunctionName), array($this->request->allParams));
            }
        }
        throw new RestApiUndefinedFunctionException($this->action);
    }

    protected function loadFunctionNames()
    {
        $methodPrefix = $this->routePrefixes[$this->request->method];
        if (NameFormatter::isCamelCase($this->action)) {
            $this->ccFunctionName = $methodPrefix . ucfirst($this->action);
            $this->usFunctionName = NameFormatter::toSnakeCase($this->ccFunctionName);
        }

        if (NameFormatter::isSnakeCase($this->action)) {
            $this->usFunctionName = $methodPrefix . "_" . $this->action;
            $this->ccFunctionName = NameFormatter::toCamelCase($this->usFunctionName);
        }
    }
    */
}