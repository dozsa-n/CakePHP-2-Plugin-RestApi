<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2015. 10. 28.
 * Time: 20:48
 */

trait ApiControllerTrait {

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('index', 'doc');
        Security::setHash('sha256');
        Configure::write('debug', 2);
    }

    public function isAuthorized($user = null)
    {
        return true;
    }

    public function index()
    {
        $this->addAllowAction('get', 'test', 'args');
        $ext = strtolower($this->request->params['ext']);
        switch($ext){
            case 'doc':
                $download = isset($this->request->query['d']) ? true : false;
                $filename = 'API - Documentation';
                if($download){
                    $this->response->header("Content-Disposition: attachment; filename='{$filename}.doc'");
                }
                parent::documentation(__CLASS__, $filename, $download);
                break;
            default:
                parent::index();
                break;
        }
    }
}