<?php

/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2015. 10. 28.
 * Time: 20:43
 */
class ConstDoc
{
    /** @var array Constant names to DocComment strings. */
    private $docComments = [];

    /** Constructor. */
    public function __construct($clazz)
    {
        $this->parse(new \ReflectionClass($clazz));
    }

    /** Parses the class for constant DocComments. */
    private function parse(\ReflectionClass $clazz)
    {
        $content = file_get_contents($clazz->getFileName());
        $tokens = token_get_all($content);
        $doc = null;
        $isConst = false;
        foreach ($tokens as $token) {
            @list($tokenType, $tokenValue) = $token;

            switch ($tokenType) {
                // ignored tokens
                case T_WHITESPACE:
                case T_COMMENT:
                    break;

                case T_DOC_COMMENT:
                    $doc = $tokenValue;
                    break;

                case T_CONST:
                    $isConst = true;
                    break;

                case T_STRING:
                    if ($isConst) {
                        $this->docComments[$tokenValue] = self::clean($doc);
                    }
                    $doc = null;
                    $isConst = false;
                    break;

                // all other tokens reset the parser
                default:
                    $doc = null;
                    $isConst = false;
                    break;
            }
        }
    }

    /** Returns an array of all constants to their DocComment. If no comment is present the comment is null. */
    public function getDocComments()
    {
        return $this->docComments;
    }

    /** Returns the DocComment of a class constant. Null if the constant has no DocComment or the constant does not exist. */
    public function getDocComment($constantName)
    {
        if (!isset($this->docComments)) {
            return null;
        }

        return $this->docComments[$constantName];
    }

    /** Cleans the doc comment. Returns null if the doc comment is null. */
    private static function clean($doc)
    {
        if ($doc === null) {
            return null;
        }

        $result = null;
        $lines = preg_split('/\R/', $doc);
        foreach ($lines as $line) {
            $line = trim($line, "/* \t\x0B\0");
            if ($line === '') {
                continue;
            }

            if ($result != null) {
                $result .= ' ';
            }
            $result .= $line;
        }
        return $result;
    }
}

trait RestApiControllerTrait
{

    protected static $ignoreClass = array('Controller', 'AppController');
    protected static $pregMatchValidate = '/^(?P<method>post|get|put|delete)(?P<action>[a-zA-Z0-9]+)$/';

    protected function ignoreClass($methods){
        $result = array();
        foreach ($methods as $key => $method) {
            $returnValue = preg_match(self::$pregMatchValidate, $method->name, $matches);
            if ($returnValue && !in_array($method->class, self::$ignoreClass) ) {
                $result[] = array(
                    'name' => $method->name,
                    'method' => $matches['method'],
                    'action' => lcfirst($matches['action'])
                );
            }
        }
        return $result;
    }

    protected function setSecurityMethodPrefix($class = null){
        if(empty($class)){
            $class = get_class($this);
        }
        $class = new ReflectionClass($class);
        $publicMethods = $class->getMethods(ReflectionMethod::IS_PUBLIC);
        $public = $this->ignoreClass($publicMethods);
        $protectedMethods = $class->getMethods(ReflectionMethod::IS_PROTECTED);
        $protected = $this->ignoreClass($protectedMethods);
        return compact('public', 'protected');
    }

    protected static function __getClassMethods($class = __CLASS__, $onlyApiFunction = true)
    {
        $rClass = new ReflectionClass($class);
        $methods = $rClass->getMethods();
        $result = array();
        if ($onlyApiFunction) {
            foreach ($methods as $key => $method) {
                $rMethod = new ReflectionMethod($method->class, $method->name);
                $returnValue = preg_match(self::$pregMatchValidate, $method->name, $matches);
                //if ($returnValue && !in_array($method->class, self::$ignoreClass) && ($rMethod->isPublic() || $rMethod->isProtected())) {
                if ($returnValue && !in_array($method->class, self::$ignoreClass) && $rMethod->isPublic()) {
                    $result[] = array(
                        'class' => $method->class,
                        'doc' => $rMethod->getDocComment(),
                        'name' => $method->name,
                        'method' => $matches[1],
                        'action' => $matches[2],
                        'access' => ($rMethod->isPublic() ? false : true)
                    );
                }
            }
            return $result;
        }
        return $methods;
    }

    protected function documentation($class = __CLASS__, $filename = 'API - Documentation', $download = false)
    {
        if (Configure::read('debug') == 2) {
            $methods = $this->__getClassMethods($class, true);
            $constants = $this->__getClassConstants('RestApiErrorException', false);
            $ConstDoc = new ConstDoc('RestApiErrorException');
            $constantDoc = $ConstDoc->getDocComments();
            $this->layout = 'ajax';
            $this->autoRender = false;
            echo '<html>';
            echo '<head>';
            echo '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
            echo '<title>' . $filename . '</title>';
            echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">';
            echo '<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">';
            echo '</head>';
            echo '<body>';
            echo '<div class="container well well-sm">';
            echo '<h1>API - Documentation</h1>';
            echo '<div class="panel panel-info">';
            echo '<div class="panel-heading"><h3 class="panel-title">Constants</h3></div>';
            echo '<div class="panel-body">';
            foreach ($constants as $property => $value) {
                echo '<div>';
                if(isset($constantDoc[$property])){
                    echo '<br>';
                    echo "<div><h4>Doc: ".$constantDoc[$property]."</h4></div>";
                }
                echo '<div><span>const <strong>' . $property . ' = ' . $value . '</strong></span></div>';
                echo '</div>';
            }
            echo '</div>';
            echo '</div>';

            echo '<div class="panel panel-info">';
            echo '<div class="panel-heading"><h3 class="panel-title">Method</h3></div>';
            echo '<div class="panel-body">';
            foreach ($methods as $method) {
                echo '<div>';
                $doc = !empty($method['doc']) ? nl2br($method['doc']) : "Empty";
                echo '<h4>' . $doc . '</h4>';
                echo '<div><span>Controller:action: <strong>' . $method['class'] . '->' . $method['name'] . '</strong></span></div>';
                echo '<div><span>API Method: <strong>' . strtoupper($method['method']) . '</strong></span></div>';
                echo '<div><span>API Action: <strong>' . lcfirst($method['action']) . '</strong></span></div>';
                #echo '<div><span>Access Action: <strong>' . ( $method['access'] ? 'Bejelentkezés köteles' : 'Nem köteles bejelentkezni') . '</strong></span></div>';
                echo '</div>';
                echo '<hr>';
            }
            echo '</div>';
            echo '</div>';

            echo '</div>';
            echo '</body>';
            echo '</html>';
        } else {
            echo "Only debug mode";
        }
        if (!$download) {
            exit(0);
        }
    }

    protected static function __getClassConstants($class = __CLASS__, $onlyApiFunction = true)
    {
        $rClass = new ReflectionClass($class);
        $constants = $rClass->getConstants();
        if ($onlyApiFunction) {
            $result = array();
            foreach ($constants as $property => $value) {
                debug($rClass->getConstant($property)->getDocComment());
                /*
                $prop = new ReflectionProperty($class, $property);

                $result[] = array(
                    'doc' => $prop->getDocComment(),
                    'property' => $property,
                    'value' => $value
                );
                */
            }
            return $result;
        }
        return $constants;
    }

    protected function isApiAuthorized()
    {
        if (in_array($this->action, $this->allowAction[strtolower($this->method)])) {
            return true;
        } else {
            $this->checkMissingParams(array("token"));

            if ($this->__loginToToken($this->allParams->token) && (boolean)AuthComponent::user()) {
                return true;
            } else {
                throw new RestApiUnauthorizedException();
            }
        }
    }
}