<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class Utility {
    /**
     * Random karakterek generálása
     * @param integer $n hash kód hossza
     * @param boolean $misc használjon speciális karaktereket is
     * @return string        
     */
    public static function makehash($n = 16, $misc = false) {
        $x = '';
        for ($i = 1; $i <= $n; $i++) {
            $guess = mt_rand(1, 10);
            switch ($guess) {
                case 1:
                case 2:
                    $x.= chr(mt_rand(48, 57)); // number 20%
                    break;
                case 3:
                case 4:
                    if ($misc) {
                        $x.= chr(mt_rand(33, 46)); // misc 20%
                        break;
                    } else {
                        $x.= self::makehash(1, false);
                    }
                    break;
                case 5:
                case 6:
                    $x.= chr(mt_rand(65, 90)); // A-Z 20%
                    break;
                default:
                    $x.= chr(mt_rand(97, 122)); // a-z 40%
                    break;
            }
        }
        return $x;
    }
}