<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2015. 10. 29.
 * Time: 22:10
 */

App::uses('NameFormatter', 'RestApi.Lib/AbstractData');
App::uses('HttpParams', 'RestApi.Lib/AbstractData');
App::uses('FileParams', 'RestApi.Lib/AbstractData');

class RestApiRequest implements ArrayAccess
{
    /**
     * HTTP Method fajták
     * @var array
     */
    protected $routePrefixes = array(
        'POST' => 'post',
        'GET' => 'get',
        'DELETE' => 'delete',
        'PUT' => 'put'
    );

    public static $options = array(
        'caseType' => NameFormatter::CAMEL_CASE,
        'functionArgs' => true,
        'actionName' => 'action'
    );
    /*
    public static $caseType = NameFormatter::CAMEL_CASE;
    public static $functionArgs = true;

    public $actionName = "action";
    */

    protected $method;
    protected $action;
    protected $getParams;
    protected $postParams;
    protected $putParams;
    protected $deleteParams;
    protected $fileParams;
    protected $allParams;

    public function __construct($options = array())
    {
        $restApiSettings = Configure::read('RestApi.Settings.controller');
        self::$options = am(self::$options, $restApiSettings, $options);
        $this->routePrefixes = Configure::read('RestApi.Settings.routePrefixes');

        $this->loadMethod();
        if (self::$options['functionArgs']) {
            $request = new CakeRequest();
            $this->allParams = array_merge_recursive($request->query, $request->data);
        } else {
            $this->loadParams();
        }
        $this->loadAction();
    }

    public function __get($key)
    {
        switch ($key) {
            case '_action':
                return $this->action;
                break;
            case '_method':
                return $this->method;
                break;
            case '_routePrefixes':
                return $this->routePrefixes;
                break;
            case '_allParams':
                return $this->allParams;
                break;
            case '_fileParams':
                return $this->fileParams;
                break;
            default:
                return isset($this->allParams[$key]) ? $this->allParams[$key] : NULL;
                break;
        }
    }

    public function __set($key, $value = NULL)
    {
        $this->allParams[$key] = $value;
    }

    public function setParams($key, &$variable)
    {
        switch ($key) {
            case 'method':
                $variable = $this->method;
                break;
            case 'action':
                $variable = $this->action;
                break;
            case 'allParams':
                $variable = $this->allParams;
                break;
            case 'fileParams':
                $variable = $this->fileParams;
                break;
            case 'routePrefixes':
                $variable = $this->routePrefixes;
                break;
        }
    }

    protected function loadMethod()
    {
        $this->method = strtoupper($_SERVER['REQUEST_METHOD']);
    }

    protected function loadAction()
    {
        $actionName = self::$options['actionName'];

        if (!isset($this->allParams[$actionName])) {
            throw new RestApiUndefinedActionException('Undefined');
        } else {
            $this->action = $this->allParams[$actionName];
            unset($this->allParams[$actionName]);
        }

    }

    protected function loadGetParams()
    {
        $this->getParams = new HttpParams($_GET, self::$options['caseType']);
    }

    protected function loadPostParams()
    {
        $this->getParams = new HttpParams($_POST, self::$options['caseType']);
    }

    protected function loadPutParams()
    {
        $params = array();
        parse_str(file_get_contents("php://input"), $params);
        $this->putParams = new HttpParams($params, self::$options['caseType']);
    }

    protected function loadDeleteParams()
    {
        $params = array();
        parse_str(file_get_contents("php://input"), $params);
        $this->deleteParams = new HttpParams($params, self::$options['caseType']);
    }

    protected function loadAllParams()
    {
        $putAndDelete = array();
        parse_str(file_get_contents("php://input"), $putAndDelete);
        $this->allParams = new HttpParams(array_merge($_POST, $_GET, $putAndDelete), self::$options['caseType']);
    }

    protected function loadFileParams()
    {
        $this->fileParams = new FileParams(array(), self::$options['caseType']);
    }

    protected function loadParams()
    {
        $this->loadGetParams();
        $this->loadPostParams();
        $this->loadPutParams();
        $this->loadDeleteParams();
        $this->loadAllParams();
        $this->loadFileParams();
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Whether a offset exists
     * @link http://php.net/manual/en/arrayaccess.offsetexists.php
     * @param mixed $offset <p>
     * An offset to check for.
     * </p>
     * @return boolean true on success or false on failure.
     * </p>
     * <p>
     * The return value will be casted to boolean if non-boolean was returned.
     */
    public function offsetExists($offset)
    {
        return isset($this->allParams[$offset]);
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to retrieve
     * @link http://php.net/manual/en/arrayaccess.offsetget.php
     * @param mixed $offset <p>
     * The offset to retrieve.
     * </p>
     * @return mixed Can return all value types.
     */
    public function offsetGet($offset)
    {
        return isset($this->allParams[$offset]) ? $this->allParams[$offset] : NULL;
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to set
     * @link http://php.net/manual/en/arrayaccess.offsetset.php
     * @param mixed $offset <p>
     * The offset to assign the value to.
     * </p>
     * @param mixed $value <p>
     * The value to set.
     * </p>
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->allParams[] = $value;
        } else {
            $this->allParams[$offset] = $value;
        }
    }

    /**
     * (PHP 5 &gt;= 5.0.0)<br/>
     * Offset to unset
     * @link http://php.net/manual/en/arrayaccess.offsetunset.php
     * @param mixed $offset <p>
     * The offset to unset.
     * </p>
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->allParams[$offset]);
    }
}