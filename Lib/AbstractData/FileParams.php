<?php

App::uses('NameFormatter', 'RestApi.Lib/AbstractData');

class FileParams extends ArrayObject
{
    public static $type = 'both';
    public function __construct($files = false, $type = 'both')
    {
        self::$type = $type;
        if (!$files) {
            $files = $_FILES;
        }

        foreach ($files as $key => $file) {
            if ($file["error"]) {
                continue;
            }

            $value = (object)array(
                "originalName" => $file["name"],
                "original_name" => $file["name"],
                "temporaryName" => $file["tmp_name"],
                "temporary_name" => $file["tmp_name"],
                "type" => $file["type"],
                "size" => $file["size"]
            );

            $this[$key] = $value;
            switch(self::$type){
                case NameFormatter::CAMEL_CASE:
                    if (NameFormatter::isSnakeCase($key)) {
                        $this[NameFormatter::toCamelCase($key)] = $value;
                    }
                    break;
                case NameFormatter::SNAKE_CASE:
                    if (NameFormatter::isCamelCase($key)) {
                        $this[NameFormatter::toSnakeCase($key)] = $value;
                    }
                    break;
                default:
                    if (NameFormatter::isSnakeCase($key)) {
                        $this[NameFormatter::toCamelCase($key)] = $value;
                    } else if (NameFormatter::isCamelCase($key)) {
                        $this[NameFormatter::toSnakeCase($key)] = $value;
                    }
                    break;
            }
        }
    }

    public function __get($key)
    {
        return isset($this[$key]) ? $this[$key] : NULL;
    }

}
