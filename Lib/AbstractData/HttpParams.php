<?php
App::uses('NameFormatter', 'RestApi.Lib/AbstractData');

class HttpParams extends ArrayObject
{
    public static $type = 'both';
    public function __construct($params = array(), $type = 'both')
    {
        parent::__construct(array());
        self::$type = $type;
        $this->addAll($params);
    }

    public function addAll($params)
    {
        foreach ($params as $key => $value) {
            $this->$key = $value;
        }
    }

    public function __set($key, $value)
    {
        //$this[$key] = $value;
        switch(self::$type){
            case NameFormatter::CAMEL_CASE:
                if (NameFormatter::isSnakeCase($key)) {
                    $this[NameFormatter::toCamelCase($key)] = $value;
                }
                break;
            case NameFormatter::SNAKE_CASE:
                if (NameFormatter::isCamelCase($key)) {
                    $this[NameFormatter::toSnakeCase($key)] = $value;
                }
                break;
            default:
                if (NameFormatter::isSnakeCase($key)) {
                    $this[NameFormatter::toCamelCase($key)] = $value;
                } else if (NameFormatter::isCamelCase($key)) {
                    $this[NameFormatter::toSnakeCase($key)] = $value;
                }
                break;
        }
    }

    public function __get($key)
    {
        return isset($this[$key]) ? $this[$key] : NULL;
    }
}

?>