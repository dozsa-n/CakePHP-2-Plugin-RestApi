<?php

/**
 * Class used to format strings. Handles the two main formattng
 * conventions: camelCase and snake_case
 */
final class NameFormatter
{
    const CAMEL_CASE = "camel";
    const SNAKE_CASE = "snake";

    /**
     * Checks if the input string is snake_case
     * @param string $input The input string
     * @return boolean True if $input was snake_case, false otherwise
     */
    public static function isSnakeCase($input)
    {
        return preg_match("/^[a-z]+(_[a-z]+)*$/", $input);
    }

    /**
     * Checks if the input string is camelCase
     * @param string $input The input string
     * @return boolean True if $input was camelCase, false otherwise
     */
    public static function isCamelCase($input)
    {
        return preg_match("/^[a-z]+([A-Z][a-z]+)*$/", $input);
    }

    /**
     * Converts the snake_case input string to camelCase
     * @param string $input The snake_case input string
     * @return string The input string in camelCase format
     */
    public static function toCamelCase($input)
    {
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $input))));
    }

    /**
     * Converts the camelCase input string to snake_case
     * @param string $input The camelCase input string
     * @return string The input string in snake_case format
     */
    public static function toSnakeCase($input)
    {
        return preg_replace_callback('/[A-Z]/', create_function('$match', 'return "_" . strtolower($match[0]);'), $input);
    }

    /**
     * Converts the input string to the given format, or if unable to
     * do so, returns it in it's original format
     * @param string $input The input string
     * @param string $case The desired output format
     * (use the CAMEL_CASE or SNAKE_CASE constants of the MeowFormatter class, if possible)
     * @return string The formated output (or the original, if formatting was not possible)
     */
    public static function to($input, $case)
    {
        $case = strtolower($case);
        if ($case == self::CAMEL_CASE) {
            return self::isCamelCase($input) ? $input : self::toCamelCase($input);
        } else if ($case == self::SNAKE_CASE) {
            return self::isSnakeCase($input) ? $input : self::toSnakeCase($input);
        }
        return $input;
    }

    private function __construct()
    {
    }
}

?>