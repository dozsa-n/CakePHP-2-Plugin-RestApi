<?php

/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2015. 10. 28.
 * Time: 20:42
 */
class RestApiErrorException extends Exception {
    /**
     * Nem támogaott Http Metodus
     */
    const ERROR_CODE_UNSUPPORTED_HTTP_METHOD = 1000;
    /**
     * Ismeretlen action paraméter
     */
    const ERROR_CODE_UNDEFINED_ACTION = 1001;
    /**
     * Ismeretlen fügvény
     */
    const ERROR_CODE_UNDEFINED_FUNCTION = 1002;
    /**
     * Hiányzó változók
     */
    const ERROR_CODE_MISSING_PARAMS = 1003;
    /**
     * Nem vagy bejeletkezve
     */
    const ERROR_CODE_UNAUTHORIZED = 1004;
    const ERROR_CODE_USE_ERROR_CODE = 1005;
    /**
     * Nem létező tmp fájl
     */
    const ERROR_CODE_NO_TEMPLATE_FILE_EXISTS = 1006;
    /**
     * Validálás hiba
     */
    const ERROR_CODE_VALIDATE_CODE = 1007;
    /**
     * Nincs visszatéritési érték a meghívott fügvénynek
     */
    const ERROR_CODE_NO_RETURN_VALUE_CODE = 1008;
    /**
     * Nincs értéke
     */
    const ERROR_CODE_EMPTY_VALUE = 1009;
    /**
     * Ha nincs filed
     */
    const ERROR_CODE_NO_FILED_EXISTS = 1010;

    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class RestApiUnsupportedHttpMethodException extends RestApiErrorException {
    public function __construct($method = 'undefined', $message = "Unsupported HTTP method %s", $code = self::ERROR_CODE_UNSUPPORTED_HTTP_METHOD, RestApiErrorException $previous = null) {
        $message = sprintf($message, $method);
        parent::__construct($message, $code, $previous);
    }
}

class RestApiUndefinedActionException extends RestApiErrorException {
    public function __construct($action = "", $message = "Undefined action! You need to provide: '%s' parameter to determine which function to call!", $code = self::ERROR_CODE_UNDEFINED_ACTION, RestApiErrorException $previous = null) {
        $message = sprintf($message, $action);
        parent::__construct($message, $code, $previous);
    }
}

class RestApiUndefinedFunctionException extends RestApiErrorException {
    // Redefine the exception so message isn't optional
    public function __construct($function = '', $message = "Undefined function: %s", $code = self::ERROR_CODE_UNDEFINED_FUNCTION, RestApiErrorException $previous = null) {
        $message = sprintf($message, $function);
        parent::__construct($message, $code, $previous);
    }
}

class RestApiMissingParamsException extends RestApiErrorException {
    public $missingParams = array();

    // Redefine the exception so message isn't optional
    public function __construct($missingParams = array(), $message = "Missing params: %s", $code = self::ERROR_CODE_MISSING_PARAMS, RestApiErrorException $previous = null) {
        $arrayToString = implode(", ", $missingParams);
        parent::__construct(sprintf($message, $arrayToString), $code, $previous);
    }
}

class RestApiUnauthorizedException extends RestApiErrorException {
    // Redefine the exception so message isn't optional
    public function __construct($message = "No logged in", $code = self::ERROR_CODE_UNAUTHORIZED, RestApiErrorException $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class RestApiUseErrorCodeException extends RestApiErrorException {
    // Redefine the exception so message isn't optional
    public function __construct($message = "Use error code", $code = self::ERROR_CODE_USE_ERROR_CODE, RestApiErrorException $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class RestApiNoTemplateFileExistsException extends RestApiErrorException {
    // Redefine the exception so message isn't optional
    public function __construct($filename = '', $message = "No file exists: %s", $code = self::ERROR_CODE_NO_TEMPLATE_FILE_EXISTS, RestApiErrorException $previous = null) {
        $message = sprintf($message, $filename);
        parent::__construct($message, $code, $previous);
    }
}

class RestApiValidateException extends RestApiErrorException {
    // Redefine the exception so message isn't optional
    public function __construct($filed = '', $message = "Not valid filed value: %s", $code = self::ERROR_CODE_VALIDATE_CODE, RestApiErrorException $previous = null) {
        $message = sprintf($message, $filed);
        parent::__construct($message, $code, $previous);
    }
}

class RestApiNoReturnValueException extends RestApiErrorException {
    // Redefine the exception so message isn't optional
    public function __construct($message = "No return value", $code = self::ERROR_CODE_NO_RETURN_VALUE_CODE, RestApiErrorException $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}

class RestApiEmptyValueException extends RestApiErrorException {
    // Redefine the exception so message isn't optional
    public function __construct($field = null, $message = "Empty value", $code = self::ERROR_CODE_EMPTY_VALUE, RestApiErrorException $previous = null) {
        if ($field) {
            if (is_array($field)) {
                $field = implode(', ', $field);
            }
            $message = sprintf('%s: (%s)', $message, $field);
        }
        parent::__construct($message, $code, $previous);
    }
}

class RestApiResponseException extends RestApiErrorException {
    public $response = array();

    // Redefine the exception so message isn't optional
    public function __construct($message = "", $response = array(), $code = 0, RestApiErrorException $previous = null) {
        if (isset($response['success'])) {
            unset($response['success']);
        }
        $this->response = $response;
        parent::__construct($message, $code, $previous);
    }

    public function getResponse() {
        return $this->response;
    }
}

class RestApiErrorCodeException extends RestApiErrorException {

    // Redefine the exception so message isn't optional
    public function __construct($code, $message = '', RestApiErrorException $previous = null) {

        parent::__construct($message, $code, $previous);
    }

    public function getResponse() {
        return array();
    }
}