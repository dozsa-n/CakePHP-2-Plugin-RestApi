<?php
/**
 * Created by PhpStorm.
 * User: dnorbert
 * Date: 15. 10. 29.
 * Time: 13:15
 */

class ValidateComponent extends Component{
    protected $controller = null;

    public $validates = [
        'string' => '[a-zA-Z]+',
        'integer' => '[0-9]+',
        'special' => '[.\\+*?[^]/$(){}=!<>|:-]'
    ];

    public function initialize(Controller $controller)
    {
        //parent::initialize($controller);
        $this->controller = $controller;
    }
}