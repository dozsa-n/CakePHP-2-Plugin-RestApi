<?php
/**
 * Created by PhpStorm.
 * User: dnorbert
 * Date: 15. 10. 30.
 * Time: 10:08
 */

App::uses('NameFormatter', 'RestApi.Lib/AbstractData');
App::uses('RestApiRequest', 'RestApi.Lib');

class CallFunctionComponent extends Component{
    /**
     * HTTP Method fajták
     * @var array
     */
    /*
    protected $routePrefixes = array(
        'POST' => 'post',
        'GET' => 'get',
        'DELETE' => 'delete',
        'PUT' => 'put'
    );
    */

    public $settings = array(
        'caseType' => NameFormatter::CAMEL_CASE,
        'functionArgs' => true,
        'actionName' => 'action'
    );

    public $functionName;
    protected $request;
    protected $controller;

    public function __construct(ComponentCollection $collection, $settings = array())
    {
        $restApiSettings = Configure::read('RestApi.Settings.controller');
        $this->settings = am($this->settings, $restApiSettings, $settings);
    }

    public function initialize(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function startup(Controller $controller)
    {
        $this->controller = $controller;
    }

    public function build(RestApiRequest $request)
    {
        $this->request = $request;
        $this->loadFunctionNames();
        return $this->callFunction();
    }

    protected function checkParamArguments($params)
    {
        $required = $params['required'];
        $params = $params['params'];
        $allParams = $this->request->_allParams;


        if(count($params) > 0 && count($allParams) === 0){
            throw new RestApiMissingParamsException($required);
        }

        $allParams = array_keys($allParams);
        $missingParams = array_diff($required, $allParams);
        if (!empty($missingParams)) {
            throw new RestApiMissingParamsException($missingParams);
        }
    }

    protected function createSortArguments($params)
    {
        $result = array();
        foreach ($params as $key => $param) {
            if(isset($this->request->_allParams[$param])) {
                $result[$param] = $this->request->_allParams[$param];
            }
        }
        return $result;
    }

    protected function callFunctionArguments()
    {
        $class = get_class($this->controller);
        if (method_exists($class, $this->functionName)) {
            $params = HttpUtility::getFuncArgNames($class, $this->functionName, true);
            $this->checkParamArguments($params);
            return call_user_func_array(array($this->controller, $this->functionName), $this->createSortArguments($params['params']));
        }else {
            throw new RestApiUndefinedFunctionException($this->functionName);
        }
    }

    protected function callFunction()
    {
        if ($this->settings['functionArgs']) {
            return $this->callFunctionArguments();
        } else {
            $class = get_class($this->controller);
            if (method_exists($class, $this->functionName)) {
                return call_user_func_array(array($this->controller, $this->functionName), array($this->request->_allParams));
            }else {
                throw new RestApiUndefinedFunctionException($this->functionName);
            }
        }
    }

    protected function loadFunctionNames()
    {

        $methodPrefix = $this->request->_routePrefixes[$this->request->_method];
        if (NameFormatter::isCamelCase($this->request->_action)) {
            $this->functionName = $methodPrefix . ucfirst($this->request->_action);
        }

        if (NameFormatter::isSnakeCase($this->request->_action)) {
            $this->functionName = $methodPrefix . "_" . $this->request->_action;
            $this->functionName = NameFormatter::toCamelCase($this->functionName);
        }
    }
}