<?php

/**
 * Created by PhpStorm.
 * User: dnorbert
 * Date: 15. 10. 28.
 * Time: 16:40
 */
App::uses('AppController', 'Controller');

App::uses('RestApiControllerTrait', 'RestApi.Lib/Trait');


include_once App::pluginPath('RestApi') . 'Lib' . DS . 'Exception' . DS . 'RestApiException.php';

App::uses('NameFormatter', 'RestApi.Lib/AbstractData');
App::uses('HttpParams', 'RestApi.Lib/AbstractData');
App::uses('FileParams', 'RestApi.Lib/AbstractData');
App::uses('RestApiRequest', 'RestApi.Lib');
App::uses('Utility', 'RestApi.Lib');
App::uses('HttpUtility', 'RestApi.Lib');
App::uses('Security', 'Utility');
//App::uses('CakeEventListener', 'Event');
set_time_limit(60);


interface RestApiCallbacks {
    public function beforeResponse($response = array());

    public function afterResponse($response = array());
}

abstract class RestApiController extends Controller implements RestApiCallbacks/*, CakeEventListener*/
{
    use RestApiControllerTrait;
    /*
    public function implementedEvents() {
        return array(
            'RestApi.Request.beforeResponse' => 'beforeResponse',
            'RestApi.Request.afterResponse' => 'afterResponse',
        );
    }
    */

    public $components = array(
        'RequestHandler',
        'Auth' => array(
            'authorize' => 'Controller',
            'authenticate' => array(
                'Form' => array(
                    'userModel' => 'User',
                    'fields' => array(
                        'username' => 'email',
                        'password' => 'password'
                    )
                )
            )
        ));

    /**
     * Amiket login nélkül is el érünk kivülről
     * @var array
     */
    protected $allowAction = array(
        'post' => array(
            'login',
            'register'
        ),
        'get' => array(
            'logout',
            'loggedIn'
        )
    );

    protected $routePrefixes;
    protected $action;
    protected $method;
    protected $allParams;
    protected $RestApiRequest;

    protected static $userId = null;

    public function beforeFilter() {
        $this->Session = $this->Components->load('Session');
        $this->RequestHandler = $this->Components->load('RequestHandler');
        $this->Cookie = $this->Components->load('Cookie');
        $this->CallFunction = $this->Components->load('RestApi.CallFunction');
        //$this->addPublicMethodToAllow();

        Security::setHash('sha256');

        $this->Auth->allow('index', 'doc');
        parent::beforeFilter();
        if (AuthComponent::user()) {
            self::$userId = AuthComponent::user('id');
        }
    }

    public function isAuthorized($user = null) {
        return true;
    }

    protected function addPublicMethodToAllow() {
        $methods = $this->setSecurityMethodPrefix();
        $publicMethods = $methods['public'];
        $result = [];
        foreach ($publicMethods as $method) {
            $result[$method['method']][] = $method['action'];
        }
        foreach ($result as $method => $actions) {
            call_user_func_array([$this, 'addAllowAction'], array_merge([$method], $actions));
        }
    }

    public function index() {
        $ext = strtolower($this->request->params['ext']);
        switch ($ext) {
            case 'doc':
                $download = isset($this->request->query['d']) ? true : false;
                $filename = 'API - Documentation';
                if ($download) {
                    $this->response->header("Content-Disposition: attachment; filename='{$filename}.doc'");
                }
                $this->documentation(get_class($this), $filename, $download);
                break;
            default:
                $this->_index();
                break;
        }
    }

    public function _index() {
        try {
            $this->RestApiRequest = new RestApiRequest();
            $this->RestApiRequest->setParams('method', $this->method);
            $this->RestApiRequest->setParams('action', $this->action);
            $this->RestApiRequest->setParams('allParams', $this->allParams);
            $this->RestApiRequest->setParams('routePrefixes', $this->routePrefixes);

            $data = $this->route();
            $returnType = gettype($data);
            if ($returnType === 'NULL') {
                throw new RestApiNoReturnValueException();
            } else {
//                if (!array_key_exists('data', $data)) {
//                    $this->onResponse(true, compact('data'));
//                } else {
                $this->onResponse(true, $data);
//                }
            }
        } catch (RestApiUnsupportedHttpMethodException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (RestApiUndefinedActionException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (RestApiUndefinedFunctionException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (RestApiMissingParamsException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (RestApiUnauthorizedException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (RestApiUseErrorCodeException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (RestApiNoTemplateFileExistsException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (RestApiEmptyValueException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (RestApiResponseException $e) {
            $response = $e->getResponse();
            $response = array_merge($response, array(
                'message' => $e->getMessage(),
                'errorCode' => $e->getCode()
            ));
            $this->onResponse(false, $response);
            return;
        } catch (RestApiValidateException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (RestApiErrorException $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        } catch (Exception $e) {
            $this->onResponse(false, array('message' => $e->getMessage(), 'errorCode' => $e->getCode()));
            return;
        }
    }

    public function transaction(Model &$model) {
        $db = $model->getDataSource();
        //$db->begin($model);

        $begin = function () use (&$db, &$model) {
            $db->begin($model);
        };

        $commit = function () use (&$db, &$model) {
            $db->commit($model);
        };

        $rollback = function () use (&$db, &$model) {
            $db->rollback($model);
        };

        return compact('begin', 'commit', 'rollback');
    }

    /**
     * Response előtti folyamat
     * @return bool
     */
    public function beforeResponse($response = array()) {
        return true;
    }

    /**
     * Response utáni folyamat
     * @return bool
     */
    public function afterResponse($response = array()) {
        return true;
    }

    public function addAllowAction() {
        $_action = func_get_args();
        $_method = strtoupper(array_shift($_action));

        $this->allowAction[strtolower($_method)] = array_unique(am($this->allowAction[strtolower($_method)], $_action));

        $allowAction = HttpUtility::convertRestApiAllowFunction($this->allowAction);
        $this->Auth->allowedActions = array_unique(am($this->Auth->allowedActions, $allowAction));
    }

    protected function isApiAuthorized() {
        if (!in_array($this->action, $this->allowAction[strtolower($this->method)])) {
            self::checkMissingParams(array("token"));
            if (empty($this->allParams['token'])) {
                throw new RestApiEmptyValueException('token');
            }

            if (!$this->__loginToToken($this->allParams['token']) && !(boolean)AuthComponent::user()) {
                throw new RestApiUnauthorizedException();
            }
        }
    }

    public function route() {
        if (!isset($this->routePrefixes[$this->method])) {
            throw new RestApiUnsupportedHttpMethodException($this->method);
        }

        $this->isApiAuthorized();
        return $this->CallFunction->build($this->RestApiRequest);
    }

    /**
     * @TODO: Nem használt function
     * @param array $headerArray
     */
    protected function setHeaders($headerArray = array()) {
        foreach ($headerArray as $key => $value) {
            if (is_integer($key) && !is_int($key)) {
                $this->response->header($key, $value);
            } else {
                $this->response->header($value);
            }
        }
    }

    /**
     * Response amit vissza küldünk a hívó félnek
     * @param bool $success - Sikeres-e vagy sem a kérés
     * @param array $data - Mit adunk vissza a hívó félnek response data
     * @return Object
     */
    public function onResponse($success = true, $data = array()) {
        unset($data['success']);
        $default = array(
            'success' => $success,
            'message' => null,
            'errorCode' => 0,
            'loggedIn' => (boolean)AuthComponent::user(),
            'data' => null,
        );
        $response = am($default, $data);
        $response = am($response, array(
            '_serialize' => array_keys($response)
        ));
        //$this->getEventManager()->dispatch(new CakeEvent('RestApi.Request.beforeResponse', $this, $response));
        call_user_func(array($this, 'beforeResponse'), array($response));
        $this->set($response);
        //$this->getEventManager()->dispatch(new CakeEvent('RestApi.Request.afterResponse', $this, $response));
        call_user_func(array($this, 'afterResponse'), array($response));
        return;
    }

    /**
     * Megvizsgljuk hogy van e olyan paraméter amire szükségünk van
     * @param array $params - A értékek amitket vizsgálni akarunk amik köteklezőek
     * @param array $data - Ha nincs érték megadva akkor CakeRequest alap cuccokból veszi a dolgokat ha van akkor abból a többől key => value párosból
     * @param string $message - Az Exception-nak tudunk egyedi üzenetet adni
     * @throws RestApiMissingParamsException
     */
    protected static function checkMissingParams($params, $data = array(), $message = null) {
        if (empty($data)) {
            $request = new CakeRequest();
            $keys = array_keys(array_merge($request->query, $request->data));
        } else {
            $keys = array_keys($data);
        }

        $diff = array_diff($params, $keys);
        if (!empty($diff)) {
            if (!empty($message)) {
                throw new RestApiMissingParamsException($diff, $message);
            }
            throw new RestApiMissingParamsException($diff);
        }
    }

    /**
     * Raw Body visszafejtése és értelmezése használható formában
     *
     * TODO: Még nincs kész teljesen majd még át kell nézni és tesztelni
     * @return array|mixed|string
     */
    protected static function getRawBody() {
        $params = [];
        $content = file_get_contents("php://input");
        try {
            // parse_str(file_get_contents("php://input"), $params);
            $params = json_decode($content, true);
            return $params;
        } catch (Exception $e) {
            return trim($content);
        }
    }

    /**
     * @param string $template
     * @param array $data
     * @param array $options
     * @throws RestApiNoTemplateFileExistsException
     * @return Array
     */
    protected function setTemplateElement($template = NULL, $data = array(), $options = array()) {
        $this->layout = "ajax";
        $set = isset($data['set']) ? $data['set'] : array();
        unset($data['set']);

        $response = am(array(
            'data' => array(),
            'message' => null,
            'html' => null
        ), $data);

        $view = new View($this, false);
        if (empty($template)) {
            $debug_backtrace = debug_backtrace();
            $functionName = $debug_backtrace[1]["function"];
            $template = "/Elements/templates/" . $functionName;
            $path = APP . "View" . $template . $view->ext;
            if (!file_exists($path)) {
                throw new RestApiNoTemplateFileExistsException($template . $view->ext);
            }
        }

        if (!empty($set) && is_array($set)) {
            $view->set($set);
            unset($set);
        }

        $html = $view->render($template);
        $response['html'] = $html;
        return $response;
    }

    /**
     * @param string $_view
     * @param array $data
     * @param array $options
     * @throws RestApiNoTemplateFileExistsException
     * @return Array
     */
    protected function setTemplateView($_view = NULL, $data = array(), $options = array()) {
        $this->layout = "ajax";
        $set = isset($data['set']) ? $data['set'] : array();
        unset($data['set']);

        $response = am(array(
            'data' => array(),
            'message' => null,
            'html' => null
        ), $data);

        $view = new View($this, false);
        $debugBacktrace = debug_backtrace();
        if (empty($_view)) {
            $_view = Inflector::underscore($debugBacktrace[1]['function']);
        }

        $template = "/{$this->request->params['controller']}/" . $_view;
        $path = APP . "View" . $template . $view->ext;
        if (!file_exists($path)) {
            throw new RestApiNoTemplateFileExistsException($template . $view->ext);
        }


        if (!empty($set) && is_array($set)) {
            $view->set($set);
            unset($set);
        }

        $html = $view->render($_view);
        $response['html'] = $html;
        return $response;
    }

    /**
     * @param string $token - Token
     * @return bool|RestApiController::onResponse
     * @throws RestApiErrorException
     */
    protected final function __loginToToken($token) {
        $model = $this->Auth->authenticate["Form"]["userModel"];
        $this->loadModel($model);
        if ($this->{$model}->hasField("token")) {
            if ($token && $this->{$model}->hasAny(array("token" => $token))) {
                $conditions["token"] = $token;
                $UserData = $this->{$model}->find("first", compact("conditions"));
                if ($UserData && $this->Auth->login($UserData[$this->{$model}->alias])) {
                    self::$userId = AuthComponent::user('id');
                    return true;
                }
            } else {
                $this->Auth->logout();
            }
        } else {
            throw new RestApiErrorException(__d("rest_api", "No token field in User model"), RestApiErrorException::ERROR_CODE_NO_FILED_EXISTS);
        }
        return false;
    }

    /**
     * Utolsó query lekérdezése
     *
     * @param Model $model
     * @return Array
     */
    public static function __getLastQuery(&$model) {
        $dbo = $model->getDataSource();
        $logs = $dbo->getLog();
        $lastLog = end($logs['log']);
        return $lastLog['query'];
    }

    public static function __getQuery(&$model) {
        $dbo = $model->getDataSource();
        $logs = $dbo->getLog();
        return $logs['log'];
    }

    /**
     * @return string - Token kulcs
     * @throws RestApiErrorException
     */
    protected final function __getToken() {
        $model = $this->Auth->authenticate["Form"]["userModel"];
        $this->loadModel($model);
        if ($this->{$model}->hasField("token")) {
            $token = Utility::makehash(60);
            $while = true;
            while ($while) {
                if ($this->{$model}->hasAny(array("token" => $token))) {
                    $token = Utility::makehash(60);
                } else {
                    $while = false;
                    return $token;
                }
            }
        } else {
            throw new RestApiErrorException(__d("rest_api", "No token field in User model"), RestApiErrorException::ERROR_CODE_NO_FILED_EXISTS);
        }
    }

    /**
     * Be van-e léptetve a felhasználó vagy sem
     * @param HttpParams $params
     * @param FileParams $files
     * @param Boolean $return - Default: false ha true akkor nincs onResponse hanem csak return érték van egyébként onResponse
     *
     * @param String $params ->token - A bejeletkezésnél kapot token kulcs
     * @return Object
     */
    /*
    public function getLoggedIn(HttpParams $params, FileParams $files, $return = false) {
        if ($this->__loginToToken($params->token) && AuthComponent::user()) {
            if (!$return) {
                $this->onResponse(true);
                return;
            } else {
                return true;
            }
        } else {
            if (!$return) {
                $this->onResponse(false);
                return;
            } else {
                return true;
            }
        }
    }
    */
    public function __loggedIn() {
        if (empty($this->allParams['token'])) {
            return false;
        }
        if ($this->__loginToToken($this->allParams['token']) && AuthComponent::user()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Felhasználó bejelentkeztetése
     * @param HttpParams $params
     * @param FileParams $files
     * @param Boolean $return - Default: false ha true akkor nincs onResponse hanem csak return érték van egyébként onResponse
     *
     * @param String $params ->email - A felhasználó email címe
     * @param String $params ->password - A felhasználó jelszava
     * @return Object
     */
    /*
    public function postLogin(HttpParams $params, FileParams $files, $return = false)
    {
        self::checkMissingParams($this->Auth->authenticate["Form"]["fields"]);

        $model = $this->Auth->authenticate["Form"]["userModel"];

        $username = $this->Auth->authenticate["Form"]["fields"]["username"];
        $password = $this->Auth->authenticate["Form"]["fields"]["password"];

        $options['conditions'] = array(
            $username => $params[$username],
            $password => AuthComponent::password($params[$password])
        );

        $findUser = $this->{$model}->find('first', $options);
        if ($findUser && $this->Auth->login($findUser[$this->{$model}->alias])) {
            $data = AuthComponent::user();
            $token = $this->__getToken();
            $this->{$model}->id = $data["id"];
            $this->{$model}->saveField("token", $token);
            $data["token"] = $token;
            if (!$return) {
                $this->onResponse(false, [
                    'data' => $data
                ]);
                return;
            } else {
                return true;
            }
        } else {
            if (!$return) {
                $this->onResponse(false, ['message' => 'Hibás email cím vagy jelszó!']);
                return;
            } else {
                return false;
            }
        }
    }
    */

    /**
     * Login de csak háttér folyamatra van
     * TODO: Kell majd még bele a $scope változó megírása
     *
     * @param $params
     * @param bool|false $return
     * @return array|bool|null
     * @throws RestApiErrorException
     * @throws RestApiMissingParamsException
     * @throws RestApiResponseException
     */
    protected function __login($params, $return = false) {
        self::checkMissingParams($this->Auth->authenticate["Form"]["fields"]);

        $model = $this->Auth->authenticate["Form"]["userModel"];

        $fieldUsername = $this->Auth->authenticate["Form"]["fields"]["username"];
        $fieldPassword = $this->Auth->authenticate["Form"]["fields"]["password"];

        $options['conditions'] = array(
            $fieldUsername => $params[$fieldUsername],
            $fieldPassword => AuthComponent::password($params[$fieldPassword])
        );

        $findUser = $this->{$model}->find('first', $options);
        if ($findUser && $this->Auth->login($findUser[$this->{$model}->alias])) {
            $data = AuthComponent::user();
            $token = $this->__getToken();
            $this->{$model}->id = $data["id"];
            $save[$this->{$model}->alias]['token'] = $token;
            $this->{$model}->save($save, false);
            $data["token"] = $token;
            if (!$return) {
                return $data;
            } else {
                return true;
            }
        } else {
            if (!$return) {
                throw new RestApiResponseException('Hibás email cím vagy jelszó!');
            } else {
                return false;
            }
        }
    }


    /**
     * A felhasznló kijeletkeztetése
     * @param HttpParams $params
     * @param FileParams $files
     * @param Boolean $return - Default: false ha true akkor nincs onResponse hanem csak return érték van egyébként onResponse
     *
     * @param String $params ->token - A bejeletkezésnél kapot token kulcs
     * @return Object ha hibával tér vissza akkor nem volt bejetkezve
     */
    /*
    public function getLogout(HttpParams $params, FileParams $files, $return = false) {

        $model = $this->Auth->authenticate["Form"]["userModel"];
        $this->loadModel($model);
        if ($this->__loginToToken($params->token) && AuthComponent::user()) {
            $userId = $this->Auth->user("id");
            $this->{$model}->id = $userId;
            $this->{$model}->saveField("token", NULL);
            $this->Auth->logout();
            if (!$return) {
                $this->onResponse(true);
                return;
            } else {
                return true;
            }
        }
        if (!$return) {
            $this->onResponse(false);
            return;
        } else {
            return true;
        }
    }
    */

    protected function __logout() {
        $model = $this->Auth->authenticate["Form"]["userModel"];
        $this->loadModel($model);
        if ($this->__loginToToken($this->allParams['token']) && AuthComponent::user()) {
            $userId = $this->Auth->user("id");
            $this->{$model}->id = $userId;
            $save[$this->{$model}->alias]['token'] = NULL;
            $this->{$model}->save($save, false);
            $this->Auth->logout();
            return true;
        }
        return true;
    }

    /**
     * A felhasznló adatainak lekérdezése
     * @param HttpParams $params
     * @param FileParams $files
     * @param Array $return - Default: false ha true akkor nincs onResponse hanem csak return érték van egyébként onResponse
     *
     * @param String $params ->token A bejeletkezésnél kapot token kulcs
     * @return Object
     *
     * @throws RestApiErrorException
     * @throws RestApiUnauthorizedException
     */
    public function getUserData(HttpParams $params, FileParams $files, $return = false) {
        if ($this->__loginToToken($params->token) && AuthComponent::user()) {
            if (!$return) {
                $this->onResponse(true, [
                    'data' => AuthComponent::user()
                ]);
            } else {
                return AuthComponent::user();
            }
        } else {
            throw new RestApiUnauthorizedException();
        }
    }
}