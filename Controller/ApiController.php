<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

App::uses('RestApiController', 'RestApi.Controller');

/**
 * CakePHP ApiController
 * @author eclickdeveloper
 */
class ApiController extends RestApiController {

    public $uses = array("User");

    public $paginate = array(
        "limit" => 10
    );

    protected function onIonicAngularJsAddHeader() {
        $this->response->header("Access-Control-Allow-Headers", "Content-Type");
        $this->response->header("Access-Control-Allow-Methods", "GET, POST, OPTIONS");
        $this->response->header("Access-Control-Allow-Origin", "*");
    }

    public function beforeResponse() {
        $this->onIonicAngularJsAddHeader();
    }

    public function beforeFilter() {
        Configure::write('debug', 2);
        $this->addAllowAction('get', 'test');
        parent::beforeFilter();
    }

    /**
     * @param int $id
     * @param string $email
     * @param string $name
     * @return obejct
     */
    public function getTest($id, $email = null, $name = null) {
        self::checkMissingParams();
        return [
            'message' => 'Szép munka!',
            'data' => compact('id', 'email', 'name')
        ];
    }
}
