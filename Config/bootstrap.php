<?php
/**
 * Created by PhpStorm.
 * User: Norbert
 * Date: 2015. 10. 28.
 * Time: 21:04
 */

Configure::write('RestApi.Settings', array(
    'routePrefixes' => array(
        'POST' => 'post',
        'GET' => 'get',
        'DELETE' => 'delete',
        'PUT' => 'put'
    ),
    'controller' => array(
        'caseType' => 'camel',
        'functionArgs' => true,
        'actionName' => 'action'
    )
));

Router::parseExtensions();
CakePlugin::routes();